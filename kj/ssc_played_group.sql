/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50540
Source Host           : localhost:3306
Source Database       : boyi

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2016-03-28 18:23:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ssc_played_group`
-- ----------------------------
DROP TABLE IF EXISTS `ssc_played_group`;
CREATE TABLE `ssc_played_group` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `type` tinyint(4) NOT NULL COMMENT 'ssc_type.type',
  `groupName` varchar(32) CHARACTER SET utf8 NOT NULL,
  `sort` int(4) NOT NULL,
  `bdwEnable` tinyint(1) NOT NULL DEFAULT '0',
  `android` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `enable` (`enable`,`type`,`groupName`,`sort`,`bdwEnable`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=latin1 COMMENT='玩法组表';

-- ----------------------------
-- Records of ssc_played_group
-- ----------------------------
INSERT INTO `ssc_played_group` VALUES ('1', '1', '1', '五星玩法', '1', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('2', '1', '1', '三星玩法', '3', '0', '1');
INSERT INTO `ssc_played_group` VALUES ('3', '1', '1', '三星组选', '4', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('4', '1', '1', '二星直选', '5', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('5', '1', '1', '二星组选', '6', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('6', '1', '1', '定位胆', '7', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('7', '1', '1', '不定胆', '8', '1', '0');
INSERT INTO `ssc_played_group` VALUES ('8', '1', '1', '大小单双', '9', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('9', '1', '2', '任选复试', '5', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('10', '1', '2', '前二', '0', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('11', '1', '2', '前三', '1', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('12', '1', '3', '直选', '0', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('13', '1', '3', '组选', '1', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('14', '1', '3', '二码', '3', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('80', '1', '2', '前一', '0', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('16', '1', '3', '定位胆', '5', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('17', '1', '3', '不定位', '2', '1', '0');
INSERT INTO `ssc_played_group` VALUES ('18', '1', '3', '大小单双', '4', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('19', '1', '4', '任选', '0', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('20', '0', '4', '选一', '0', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('21', '1', '4', '选二', '0', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('22', '1', '4', '选三', '0', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('59', '1', '5', '三星玩法', '3', '0', '1');
INSERT INTO `ssc_played_group` VALUES ('58', '1', '5', '四星玩法', '2', '0', '1');
INSERT INTO `ssc_played_group` VALUES ('26', '1', '6', '猜冠军', '1', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('27', '0', '6', '猜冠亚军', '2', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('28', '1', '6', '猜前三名', '3', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('29', '1', '6', '定位胆选', '4', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('38', '1', '8', '任选', '0', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('39', '1', '9', '和值', '1', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('40', '1', '9', '三同号通选', '3', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('41', '1', '9', '三同号单选', '2', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('42', '1', '9', '二同号复选', '6', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('43', '1', '9', '二同号单选', '7', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('44', '1', '9', '三不同号', '5', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('45', '1', '9', '二不同号', '8', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('46', '1', '9', '三连号通选', '4', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('50', '1', '2', '单式', '2', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('66', '1', '1', '四星玩法', '2', '0', '1');
INSERT INTO `ssc_played_group` VALUES ('60', '1', '5', '三星组选', '4', '0', '1');
INSERT INTO `ssc_played_group` VALUES ('61', '1', '5', '二星直选', '5', '0', '1');
INSERT INTO `ssc_played_group` VALUES ('62', '1', '5', '二星组选', '6', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('63', '0', '5', '五星玩法', '1', '0', '1');
INSERT INTO `ssc_played_group` VALUES ('64', '1', '5', '不定胆', '7', '1', '0');
INSERT INTO `ssc_played_group` VALUES ('65', '1', '5', '大小单双', '8', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('67', '1', '6', '两面', '5', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('68', '1', '6', '龙虎', '6', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('69', '1', '6', '冠亚季选一', '7', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('70', '0', '6', '冠亚组合', '8', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('72', '1', '5', '定位胆', '6', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('73', '1', '5', '趣味', '10', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('74', '1', '1', '趣味', '11', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('75', '0', '2', '趣味型', '7', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('76', '1', '1', '任选玩法', '10', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('77', '1', '2', '定位胆', '3', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('78', '1', '2', '不定位', '4', '1', '0');
INSERT INTO `ssc_played_group` VALUES ('79', '1', '2', '任选单试', '6', '0', '0');
INSERT INTO `ssc_played_group` VALUES ('81', '1', '10', '一般玩法', '1', '0', '0');
