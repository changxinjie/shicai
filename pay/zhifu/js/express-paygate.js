var Wi = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1 ];// 加权因子   
var ValideCode = [ 1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2 ];// 身份证验证位值.10代表X  
function $$(id){
	return document.getElementById(id);
}
function clearNumber(){
	var obj = $$("mobile");
	if(obj.value=="此卡在银行预留的手机号码"){
		obj.value="";
	}
}

function checkTeleNum(val) {
	var phoneValue= $$("mobile").value.replace(/(^\s*)|(\s*$)/g,"");
	$("#mobile").val(phoneValue);
	if(0==phoneValue.length || phoneValue =='此卡在银行预留的手机号码'){
		$("#chPhoneNumber").css("display","inline-block");
		$("#chPhoneNumber").html("请输入预留在银行的手机号码！");
		setTimeout($$("mobile").focus(), 50);
		return;
	}
	
	if(!/^((13[4-9])|(147)|(15[0-2,7-9])|(18[2-3,7-8])|(13[0-2])|(145)|(15[5-6])|(186)|(133)|(153)|(18[0,9]))\d{8}$/.test(phoneValue)){
		$("#chPhoneNumber").css("display","inline-block");
		$("#chPhoneNumber").html("手机号码有误，请确认手机号码！");
		setTimeout($$("mobile").focus(), 50);
		return;
	} else {
		$("#chPhoneNumber").css("display","none");
	} 
}

function checkradio(id) {
	var i=id.indexOf("_");
	var bankcode = id.substring(i+1,id.length-2);
	$$("bankCode").value = bankcode;
	//var j=id.lastIndexOf("_");
	//var cartype=id.substring(j+1,id.length);
	//if(cartype=="2"){
	//	$$("cardType").value="1";
	//} else {
	//	$$("cardType").value="0";
	//}	
	$("#"+id).attr("checked",true);
}

function checkBindRadio(id) {
	var svalue = $("#"+id).val();
	$("#signId").val(svalue);
	$("#"+id).attr("checked",true);
}

function CheckExpressSign(url){
	var mobile = $$("mobile").value;
//	var chkNumber = $$("chkNumber").value;//验证码暂时去掉
	var providerType = $$("providerType").value;
	var cardType = $$("cardType").value;
	var bankCode = $$("bankCode").value;
	var orderKey = $$("orderKey").value;
	var dataStr = "mobile="+mobile+
//	"&chkNumber="+chkNumber+ //验证码暂时去掉
	"&providerType="+providerType+"&cardType="+cardType+
	"&bankCode="+bankCode+"&orderKey="+orderKey;
	
	$.ajax({
        type: "post",
        url: url,
        dataType:"responseText",
        data: dataStr,
        success: result
    });
}

function result(dataObj){
	var data=dataObj.replace(/(^\s*)|(\s*$)/g, "");
	if("newBank"==data){
		//跳转至绑定新银行页面
		$("#newBankImg").attr("src","images/"+$("input[name='payChannelId']:checked").attr("title")+".png");
		$("#signId").val("");
		$(".ui-security-area").show();
		$(".agreement").show();
		$(".non").css("display","none");
		return;
	}
	if("systemError"==data){
		alert("系统繁忙请稍后再试!");
		return;
	}
//	if("chkNumberError"==data){ //暂时去掉验证码功能
//		alert("验证码错误请重新输入");
//		changeNum();
//		setTimeout($$("chkNumber").focus(), 50);
//		return;
//	}
	data = eval("("+data+")");
	$(".default").css("display","");
	$(".non").css("display","none");
	$("#bindCardDiv").html("");
	if(data.length>0){
		$$("signId").value=data[0].signId;//如有签约表给默认签约ID
	}
	var htmlContent="";
	for(var i=0;i<data.length;i++) {
		if(data[i].bankNum == undefined || data[i].bankCode == undefined || data[i].signId == undefined ) {
			continue;
		}
		htmlContent=htmlContent+
		" <span class=\"long-logo fn-left\"><input type=\"radio\" name=\"bFrpId\" id=\"de01_"+data[i].bankNum+"\" onclick=\"checkBindRadio(this.id)\" value=\""+data[i].signId+"\" />" +
		"<label class=\"icon-box min\" id=\"de01_"+data[i].bankNum+"\" onclick=\"checkBindRadio(this.id)\" for=\"de01_pab\"> <img class=\"aimg\" src=\"images/"+data[i].bankCode+".png\" />" +
		"<span>**"+data[i].bankNum+"</span> </label></span>";
	}
	$("#agreementFile").css("display","none");
	$("#agreePay").attr("src","images/btnq.png");
    $("#bindCardDiv").html(htmlContent);
	$("#bindCardDiv input[type='radio']:first").attr("checked",true);
	//[{"bankCode":"ABC","bankNum":"3456","signId":5},{"bankCode":"ABC","bankNum":"3456","signId":6}]
}

//获取最终选择的通道值
function getPayChannelVal(){
	var payChannels =  document.getElementsByName("payChannelId");
	if(payChannels.length>0){
		for(var i=0;i<payChannels.length;i++){
			if(payChannels[i].checked==true){
				return payChannels[i].value;
			}
		}
	}
}

function getSmsCode(url) {
	$("#smsCode").val("");
	var signId=$("#signId").val();
	var mobileNumber = $("#mobile").val();
	
	if(mobileNumber == "") {
		alert("获取预留手机号失败！请刷新后重试!");
		return;
	}
	if(signId ==""){ //如果是新绑定的卡 则按卡类型区分 参数的校验
		if(!checkParam("getSMS")){
			return;
		}
	} 
	var realName =$("#realName").val();
	var idNo =$("#idNo").val();
	var bankNum =$("#bankNum").val();
	var cardCVV2 = $("#cardCVV2").val();
	var cardExpDate =$("#cardExpDate").val();
	var payChannelId = getPayChannelVal();
	if(payChannelId ==""){
		alert("绑定新卡失败,请重新选择要开通快捷支付的银行卡");
		return;
	}
	payChannelId=payChannelId.substr(payChannelId.lastIndexOf("|")+1);
	var bankCode = $("#bankCode").val();
	var cardType = $("#cardType").val();
	var amount = $("#amount").val();
	var dataStr ="signId="+signId+"&mobile="+mobileNumber+"&realName="+realName+
	"&idNo="+idNo+"&bankNum="+bankNum+"&cardCVV2="+cardCVV2+"&cardExpDate="+cardExpDate+
	"&payChannelId="+payChannelId+"&bankCode="+bankCode+"&cardType="+cardType+"&amount="+amount;
	
	$("#sms-validate-first").attr("disabled" , true);
	$("#sms-validate-first").css("cursor" , "default");
	
	$.ajax({
		type: "post",
		url: url,
		dataType:"responseText",
		data: dataStr,
		success: function(result){
		var data = eval("("+result+")");
			if(data.tradeNo != ""){
				$("#tradeNo").val(data.tradeNo);
			}else{
				alert(data.retMsg);
			}
		}
	});
	
	var i = 60;
	var intval = setInterval(function(){
		if(0 < i){
			$("#sms-validate-first").val(i + "秒后重新获取");
			i--;
			$("#rebank").css("display","none");
			$("#renewbank").css("display","none");
		}else{
			$("#sms-validate-first").attr("disabled", false);
			$("#sms-validate-first").val("免费获取");
			$("#sms-validate-first").css("cursor" , "pointer");
			$("#rebank").css("display","");
			$("#renewbank").css("display","");
			clearInterval(intval);
		}
	},1000);
}

function clickNext(url){
	var phoneValue= $$("mobile").value.replace(/(^\s*)|(\s*$)/g,"");
	if(0==phoneValue.length){
		$("#chPhoneNumber").css("display","inline-block");
		$("#chPhoneNumber").html("请输入预留在银行的手机号码！");
		setTimeout($$("mobile").focus(), 50);
		return;
	} else {
		$("#mobile").val(phoneValue.replace(/(^\s*)|(\s*$)/g,""));
	}
	if(!/^((13[4-9])|(147)|(15[0-2,7-9])|(18[2-3,7-8])|(13[0-2])|(145)|(15[5-6])|(186)|(133)|(153)|(18[0,9]))\d{8}$/.test(phoneValue)){
		$("#chPhoneNumber").css("display","inline-block");
		$("#chPhoneNumber").html("手机号码有误，请确认手机号码！");
		setTimeout($$("mobile").focus(), 50);
		return;
	} else {
		$("#chPhoneNumber").css("display","none");
	} 
	
//	var chNumber = $$("chkNumber").value;
//	if(chNumber ==""){
//		$("#chNumEroor").css("display","inline-block");
//		$("#chNumEroor").html("验证码不能为空");
//		setTimeout($$("chkNumber").focus(), 50);
//		return;
//	}else{
//		$("#chNumEroor").css("display","none");
//	}
	CheckExpressSign(url);
}

function checkctp(cardtp){
	$("#cardType").val(cardtp);
	checkDefaultNewBank(cardtp);
}
function checkDefaultNewBank(id){
	if(id=="1"){
		$("#Fdebit").removeClass("dis");
		$("#Fcredit").addClass("dis");
		$("#debit").removeClass("dis");
		$("#credit").addClass("dis");
		$("#credit_tab input[type='radio']:first").attr("checked",true);
		$("#bankCode").val($("#credit_tab input[type='radio']:first").attr("title"));
	}else{
		$("#Fdebit").addClass("dis");
		$("#Fcredit").removeClass("dis");
		$("#debit").addClass("dis");
		$("#credit").removeClass("dis");
		$("#debit_tab input[type='radio']:first").attr("checked",true);
		$("#bankCode").val($("#debit_tab input[type='radio']:first").attr("title"));
	}
}

function checkParam(param){
	var signId=$("#signId").val();
	var smsCode =$("#smsCode").val();
	if(signId !=""){
		if(smsCode ==""){
			$("#chSmsCodeError").css("display","inline-block");
			setTimeout($$("smsCode").focus(), 50);
			return false;
		}else {
			$("#chSmsCodeError").css("display","none");
		}
	} else {
		
		if(!validateNewBindBankInfo()){
			return false;
		}
		if($("#cardType").val()=="1"){
			
			var cardCVV2 = $("#cardCVV2").val();
			var cardExpDate =$("#cardExpDate").val();
			if(!/^\d+$/.test(cardCVV2)||cardCVV2.replace(/(^\s*)|(\s*$)/g,"").length>4||cardCVV2.replace(/(^\s*)|(\s*$)/g,"").length<3||cardCVV2==""){
				$("#chCVVError").css("display","inline-block");
				setTimeout($$("cardCVV2").focus(), 50);
				return false;
			}else{
				$("#chCVVError").css("display","none");
			}
			  if(cardExpDate.length<3){
				 $("#chCardExpDateError").css("display","inline-block");
				setTimeout($$("cardExpDate").focus(), 50);
				return false;
			  } else {
				$("#chCardExpDateError").css("display","none");
			}
		}
		
		if(param!="getSMS"){
			if(smsCode ==""){
				$("#chSmsCodeError").css("display","inline-block");
				setTimeout($$("smsCode").focus(), 50);
				return false;
			}else {
				$("#chSmsCodeError").css("display","none");
			}
		}
		
//		if($("#saveCard").attr("checked")!=true){
//			$("#chSaveCardError").css("display","inline-block");
//			return false;
//		}else {
//			$("#chSaveCardError").css("display","none");
//		}
	}
	return true;
}
$(function(){
	$("#closeMsg").click(function(){
		$(".top p").fadeOut(500);
	});
	
	$("#c_tab0 .ui-btn-text").click(function(){
		
		if(checkParam("pay")){
			document.getElementById("agreePay").disabled=true;
			$("#bank_expresspay").submit();		
			
		}
	});
});
function validateNewBindBankInfo(){
	var realName =$("#realName").val();
	var idNo =$("#idNo").val();
	var bankNum =$("#bankNum").val();
	
	idNo = idNo.replace(/(\s*)/g,"");
	$("#idNo").val(idNo);
	
	bankNum = bankNum.replace(/(\s*)/g,"");
	$("#bankNum").val(bankNum);

	if(realName=="" || realName.length>4){
		$("#chRealName").css("display","inline-block");
		setTimeout($$("realName").focus(), 50);
		return false;
	} else{
		$("#chRealName").css("display","none");
	}	
	if(!/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(idNo)){
		$("#chIdNoError").css("display","inline-block");
		setTimeout($$("idNo").focus(), 50);
		return false;
	}else{
		$("#chIdNoError").css("display","none");
	}
	if(!IdCardValidate(idNo)){
		$("#chIdNoError").css("display","inline-block");
		setTimeout($$("idNo").focus(), 50);
		return false;
	} else {
		$("#chIdNoError").css("display","none");
    }
	
	if(!/^\d+$/.test(bankNum)||bankNum.replace(/(^\s*)|(\s*$)/g,"").length>19||bankNum.replace(/(^\s*)|(\s*$)/g,"").length<13||bankNum==""){
		$("#chBankNumError").css("display","inline-block");
		setTimeout($$("bankNum").focus(), 50);
		return false;
	}else{
		$("#chBankNumError").css("display","none");
	}
	return true;
	
}

function reNewBank(){	
	$$("selcardType").value=$$("cardType").value;
	var htmlContent =$("#firstBankTableDiv").html()==""?$("#newBindBank").html():$("#firstBankTableDiv").html();//第一个tab
	$("#newBindBank").html(htmlContent);//新tab
	$("#firstBankTableDiv").html("");//清空第一tab
	checkDefaultNewBank("0");//默认选借记卡第一个银行
	$$("cardType").value="0";
	$("#credit_tab").css("display","none");//隐藏信用卡选项
	$("#debit_tab").show();//显示借记卡
	$("#xbox-mock").show();	
	$("#renewbank").css("display","none");//隐藏按钮 
	$$("isReNewBank").value="1";
	
}

//去掉字符串头尾空格   
function trim(str) {   
    return str.replace(/(^\s*)|(\s*$)/g, "");   
}  

/**  
 * 验证15位数身份证号码中的生日是否是有效生日  
 * @param idCard15 15位书身份证字符串  
 * @return  
 */  
function isValidityBrithBy15IdCard(idCard15){   
      var year =  idCard15.substring(6,8);   
      var month = idCard15.substring(8,10);   
      var day = idCard15.substring(10,12);   
      var temp_date = new Date(year,parseFloat(month)-1,parseFloat(day));   
      // 对于老身份证中的你年龄则不需考虑千年虫问题而使用getYear()方法   
      if(temp_date.getYear()!=parseFloat(year)   
              ||temp_date.getMonth()!=parseFloat(month)-1   
              ||temp_date.getDate()!=parseFloat(day)){   
                return false;   
        }else{   
            return true;   
        }   
  } 

 /**  
  * 验证18位数身份证号码中的生日是否是有效生日  
  * @param idCard 18位书身份证字符串  
  * @return  
  */  
function isValidityBrithBy18IdCard(idCard18){   
    var year =  idCard18.substring(6,10);   
    var month = idCard18.substring(10,12);   
    var day = idCard18.substring(12,14);   
    var temp_date = new Date(year,parseFloat(month)-1,parseFloat(day));   
    // 这里用getFullYear()获取年份，避免千年虫问题   
    if(temp_date.getFullYear()!=parseFloat(year)   
          ||temp_date.getMonth()!=parseFloat(month)-1   
          ||temp_date.getDate()!=parseFloat(day)){   
            return false;   
    }else{   
        return true;   
    }   
}   

/**  
 * 判断身份证号码为18位时最后的验证位是否正确  
 * @param a_idCard 身份证号码数组  
 * @return  
 */  
function isTrueValidateCodeBy18IdCard(a_idCard) {   
    var sum = 0; // 声明加权求和变量   
    if (a_idCard[17].toLowerCase() == 'x') {   
        a_idCard[17] = 10;// 将最后位为x的验证码替换为10方便后续操作   
    }   
    for ( var i = 0; i < 17; i++) {   
        sum += Wi[i] * a_idCard[i];// 加权求和   
    }   
    valCodePosition = sum % 11;// 得到验证码所位置   
    if (a_idCard[17] == ValideCode[valCodePosition]) {   
        return true;   
    } else {   
        return false;   
    }   
}

function IdCardValidate(idCard) { 
    idCard = trim(idCard.replace(/ /g, ""));   
    if (idCard.length == 15) {   
        return isValidityBrithBy15IdCard(idCard);   
    } else if (idCard.length == 18) {   
        var a_idCard = idCard.split("");// 得到身份证数组   
        if(isValidityBrithBy18IdCard(idCard)&&isTrueValidateCodeBy18IdCard(a_idCard)){   
            return true;   
        }else {   
            return false;   
        }   
    } else {   
        return false;   
    }   
} 
	
$(function(){
	
	$(".table_bottom input").click( function(){
		$("#newBankImg").attr("src","images/"+$("input[name='payChannelId']:checked").attr("title")+".png");
		$("#signId").val("");
		$("#xbox-mock").hide();
		$(".default").hide();
		$(".ui-security-area").show();
		$(".agreement").show();
		$("#rebank").css("display","");
		$("#renewbank").css("display","");
		$("#agreementFile").css("display","");
	    $("#agreePay").attr("src","images/btn.png");
	    $$("isReNewBank").value="0";
	    
	    if($("#cardType").val()=="1"){
		    $(".credit").show();
	    } else {
	    	$(".credit").hide();
	    	$("#cardCVV2").val("");
	    	$("#cardExpDate").val("");
	    	$("#yearName").val("");
	    	$("#monthName").val("");
	    	
	    }
	    
	});
	
	$("#selectOther").click(function(){
		if($("#xbox-mock").css("display")=="none"){
			$("#xbox-mock").show();			
			$("input:radio[name='FrpId']").attr("checked",false);
		}else{
			$("#xbox-mock").hide();
		}
	});
	
	$("#rebank").click(function(){
		$$("selcardType").value=$$("cardType").value;
		var htmlContent =$("#firstBankTableDiv").html();
		$("#newBindBank").html(htmlContent);
		$("#firstBankTableDiv").html("");
		checkDefaultNewBank("0");
		$$("cardType").value="0";
		$("#credit_tab").css("display","none");
		$("#debit_tab").show();
		$("#newBankImg").attr("src","images/"+$("#bankCode").val()+".png");
		$("#xbox-mock").show();	
		$("#rebank").css("display","none");
		$$("isReNewBank").value="1";
		
	});

	$(".xbox-close").click( function(){
		$("#xbox-mock").hide();
		$("#rebank").css("display","");
		$("#renewbank").css("display","");
		setTimeout($("#smsCode").val(""),50);
		var htmlContent =$("#newBindBank").html();
		$("#firstBankTableDiv").html(htmlContent);
		$("#newBindBank").html("");
		$$("cardType").value=$$("selcardType").value;
		$$("isReNewBank").value="0";
	});
	
			
	$("input:radio[name='FrpId']").click(function(){
		$("#bank_change").html('<span class="long-logo"><label class="icon-box" for="de_icbc"><img class="img" src="images/ICBC.png"></label></span>');				
			var img=$("input:radio[name='FrpId']:checked").val();
			$(".img").attr("src","images/"+img+".png");
	});
	
	$("input:radio[name='aFrpId']").click(function(){			
			var aimg=$("input:radio[name='aFrpId']:checked").val();
			$(".aimg").attr("src","images/"+aimg+".png");
	});
	
	$(".card a").click(function(){
		if($(this).index()==0){
			$(".card a").eq(1).removeClass("dis");
			$(".card a").eq(3).removeClass("dis");
		}else{
			$(".card a").eq(0).removeClass("dis");
			$(".card a").eq(2).removeClass("dis");
		}

		$(this).addClass("dis");
		var id=$(".dis").attr("id");
		if(id=='Fcredit'){			
			$$("cardType").value="1"; //通道表中的卡类型 信用卡用2表示  快捷接口中 信用卡用1表示
			$("#credit_tab input[type='radio']:first").attr("checked",true);
		}else if(id == "Fdebit"){
			$$("cardType").value="0";//通道表中的卡类型  借记卡用1表示  快捷接口中 借记卡用0表示
			$("#debit_tab input[type='radio']:first").attr("checked",true);
		}
		if($(".dis").attr("id")=="credit"||$(".dis").attr("id")=="Fcredit"){
			if($("#isReNewBank").val()=="0"){
				$(".credit").show();
			}			
			$(".ui-xbox-content table").eq(0).show();
			$(".ui-xbox-content table").eq(2).show();
			$(".ui-xbox-content table").eq(1).hide();
			$(".ui-xbox-content table").eq(3).hide();
		}else{
			if($("#isReNewBank").val()=="0"){
			$(".credit").hide();
			}
			$(".ui-xbox-content table").eq(1).show();
			$(".ui-xbox-content table").eq(3).show();
			$(".ui-xbox-content table").eq(0).hide();
			$(".ui-xbox-content table").eq(2).hide();	
		}
		
	});
		
});
