﻿ //切换支付类型
function ShowTab(id,sum){
	var tag="c_";
	$(".content").hide();
	$(".current").attr("className","nav_item");
	$("#"+id).addClass("current");
	$("#"+tag+id).show();
	//选择第一个radio
	if(id=="tab1") {
            $("#"+his_bank+" "+"input[type='radio']:first").attr("checked",true);
	 } else if(id=="tab4") {
            $("#"+b2b_his_bank+" "+"input[type='radio']:first").attr("checked",true);
	 } else if(id=="tab0"){
			 checkDefaultNewBank($("#cardType").val());
	 } else {
	    $("#"+tag+id+" "+"input[type='radio']:first").attr("checked",true);
	}
	
}

function Show(id){
	var id=id.toString();
	//var bankcode=id.substr(2);
	var str=id.split("_");
	var bankcode=str[1];
	var channelId=str[2];
	ShowBankService(channelId,bankcode);
	$("#"+id).attr("checked",true);
}

function ShowBankService(channelId,bankcode){


       $("#bank_code").attr("value",bankcode);


	var tag="_service";
	$("table[id$='"+tag+"']").hide();
	$("#"+channelId+tag).show();
}

//加载函数
$(function(){
	initPage();
	var x = 41;
	var y = 20;
	$("a.tooltip").mouseover(function(e){
		var merchantOrderId = $("#merchantOrderId").text();
		var txMoney = $("#txMoney").text();
		var productName = $("#productName").val();
		var recevierName = $("#recevierName").val();
		var customerName = $("#customerName").val();
		var customerTelephone = $("#customerTelephone").val();
		var customerEmail = $("#customerEmail").val();
		
		if(productName!=null){
		   productName=productName.replace(/(^\s*)|(\s*$)/g, "");	
		}
		
		if(recevierName!=null){
		   recevierName=recevierName.replace(/(^\s*)|(\s*$)/g, "");
			}
		
		if(customerName!=null){
			customerName=customerName.replace(/(^\s*)|(\s*$)/g, "");
			}
		if(customerTelephone!=null){
			customerTelephone=customerTelephone.replace(/(^\s*)|(\s*$)/g, "");
			}
		if(customerEmail!=null){
			customerEmail=customerEmail.replace(/(^\s*)|(\s*$)/g, "");
			}
		
		
		this.myTitle = this.title;
		this.title = "";
		var tooltip = "<table id='tooltip'><tr><td align='right'>订单号：<\/td><td>" + merchantOrderId + "<\/td><\/tr><td align='right'>订单金额：<\/td><td>"+txMoney +"<\/td><\/tr>";
		if(!(productName == null || productName == ""|| productName == "null")){
			tooltip += "<td align='right'>商品名称：<\/td><td>"+productName+"<\/td><\/tr>";
		}
		if(!(recevierName == null || recevierName == ""||recevierName == "null")){
			tooltip += "<td align='right'>收货人：<\/td><td>"+recevierName+"<\/td><\/tr>";
		}
		if(!(customerName == null || customerName == ""||customerName == "null")){
			tooltip += "<td align='right'>付款人：<\/td><td>"+customerName+"<\/td><\/tr>";
		}
		if(!(customerTelephone == null || customerTelephone == ""||customerTelephone == "null")){
			tooltip += "<td align='right'>付款人电话：<\/td><td>"+customerTelephone+"<\/td><\/tr>";
		}
		if(!(customerEmail == null || customerEmail == ""||customerEmail == "null")){
			tooltip += "<td align='right'>付款人邮箱：<\/td><td>"+customerEmail+"<\/td><\/tr>";
		}
		
		$("body").append(tooltip);  //把它追加到文档中
		var tb_width = document.all.tooltip.offsetWidth;
		var head_width = getLeft(this);
		//alert(head_width);
		$("#tooltip")
			.css({
				"top": (118) + "px",
				"left": (head_width + x - tb_width) + "px",
				"position":"absolute",
				"border":"1px solid #ccc",
				"background":"rgb(255, 255, 255)"
			}).show("fast");      
			$("#tooltip .lef").css("text-align","right");//设置x坐标和y坐标，并且显示
	}).mouseout(function(){
		this.title = this.myTitle;
		$("#tooltip").remove();   //移除
	});
});

//获取元素绝对x坐标
function getLeft(e){
	var l=e.offsetLeft;
	while(e=e.offsetParent){
		l+=e.offsetLeft;
	}
	return l;
}

//提交表单
function submitForm(formname){
	var bankCode = "";
	var chiannelId="";
	var chkObjs = $('#'+formname+" input[name='payChannelId']"); 

	for(var i=0;i<chkObjs.length;i++){
		if(chkObjs[i].checked){
			var id=chkObjs[i].id.toString();
			var str=id.split("_");
			bankCode = str[1];
			chiannelId=str[2];
			break;
		}else{
			if(i == chkObjs.length-1){ 
				alert("请选择支付银行"); 
			  return false; 
			}
			 
		}
		
	}
	//非银行卡支付不更新Cookie
	if(bankCode!="CMPAY" && bankCode!="ZYC"){
	        //保存24小时
		setCookie("tempChannelId",chiannelId,24);
	}
	var redoFlag = $("#redoFlag").val();
	if("1" == redoFlag){
		$("#"+formname).attr("target", "_self");
	}else{
		openDialog();
		$("#"+formname).attr("target", "dinpay");
	}
	
	//如果是点卡支付，则加入通道的“通道代码+dinpay”费率信息
	if('dcard_pay'==formname){
		var $s = '#'+bankCode+'_dinpayRate';
		var dinpayRate = $($s).val();
		$('#dinpayRate').val(bankCode+'|'+dinpayRate);
	}
	$("#"+formname).submit();
}
//提交B2B表单
function b2bsubmitForm(){
	var bankCode = "";
	var chiannelId="";
	var chkObjs = document.getElementsByName("payChannelId");
	for(var i=0;i<chkObjs.length;i++){
		if(chkObjs[i].checked){
			var id=chkObjs[i].id.toString();
			var str=id.split("_");
			bankCode = str[1];
			chiannelId=str[2];
			break;
		}
	}
	//非银行卡支付不更新Cookie
	if(bankCode!="CMPAY" && bankCode!="ZYC"){
	        //保存24小时
		setCookie("b2btempChannelId",chiannelId,24);
	}
	var redoFlag = $("#redoFlag").val();
	if("1" == redoFlag){
		$("#"+formname).attr("target", "_self");
	}else{
		openDialog();
		$("#"+formname).attr("target", "dinpay");
	}
	$("#"+formname).submit();
}
//打开遮罩层
function openDialog(){
	var func = "checkSuccess('PaymentCom')";
	setInterval(func, 6000);
    var winHeight=0;
    if (document.body && document.body.scrollHeight){
        winHeight = document.body.scrollHeight;
    }
    $("#fade").css("height",winHeight+"px");
        $("#fade").show();
        $("#light").show();
}
//关闭遮罩层
function closeDialog(){
        $("#fade").hide();
        $("#light").hide();
}
//设置Cookie
function setCookie(name,value,expireHours){
	var cookieString=name+"="+escape(value);
	if(expireHours>0){
		var exp=new Date();
		exp.setTime(exp.getTime()+expireHours*60*60*1000);
		cookieString=cookieString+"; expires="+exp.toGMTString();
	} 
	document.cookie=cookieString;
}
//获取Cookie
function getCookie(name){
        var strCookie=document.cookie;
        var arrCookie=strCookie.split("; ");
        for(var i=0;i<arrCookie.length;i++){
            var arr=arrCookie[i].split("=");
            if(arr[0]==name){
            	return arr[1]
    		};
        }
        return "0";
}

 function getObjectId(obj) {
	var id = null;//这个是要的id       
	if (obj) {
		id = obj.id;
	}
	return id;
}
 
//初始化
function initPage(){
		
       // var allbank = [ "ICBC", "BCOM", "SDB", "ECITIC", "ABC", "CMB", "CMBC", "CEBB","NBB","BOB",
    	//                        "CCB", "SPABANK", "BEA", "BOC", "PSBC", "CIB", "GDB", "HXB", "SPDB", "CMBC2", "ECITIC2","BHB", "HSBANK"];
        var tmpStr=getCookie("tempChannelId");
        var b2btmpStr=getCookie("b2btempChannelId");
        var dcardTmpStr=getCookie("dcardTmpBankId");
        //Cookie是否存在
        //if(tmpStr=="0"){
            //   tmpStr="ICBC";
      //  }
       // if(b2btmpStr=="0"){
             //  b2btmpStr="ICBC";
       // }
        //银行是否正确
        //var tmpFlag=$.inArray(tmpStr, allbank);
       // if(tmpFlag==-1){
        //    tmpStr="ICBC";
       // }
       // var b2btmpFlag=$.inArray(b2btmpStr, allbank);
       // if(b2btmpFlag==-1){
        //    b2btmpStr="ICBC";
       // }
     //   ShowBankService(tmpStr);
        his_bank="his_"+tmpStr;
        
        b2b_his_bank="b2b_his_"+b2btmpStr;
        dcard_his_bank="dcad_his"+dcardTmpStr;
        
       	
    //   	$("#"+his_bank).show();
   //     $("#"+his_bank).attr("checked",true);
     //   $("#"+b2b_his_bank).show();
     //   $("#"+b2b_his_bank+" "+"input:first").attr("checked",true);
        $("#"+dcard_his_bank).parent().show();
        $("#"+dcard_his_bank).attr("checked",true);
        
       
        var tempId=$("#ul_button").find("li:eq(0)").attr("id");
        if(tempId=="tab4") {
        	ShowBankService(b2btmpStr);
        }
        ShowTab(tempId,4);
}

function showPro(url){
	var wid = $(window).width();
	var hei = $(window).height();
	var lef = (wid - 600)/2+"px";
	var heii = (hei - 400)/2+"px";
	$(".terms").css({
		"left":lef,
		"top":heii
	})
	$(".bg").css({
	"opacity":0.7,
	"width":wid,
	"height":hei+200
	});
	$(".bg").css("display","block");
	document.getElementById('proess').style.display = "";
	$("#fade").hide();
    $("#light").hide();
	var dataStr="orderKey="+$("#orderKey").val();
	var isCheckBank = "isCheckBank=0";
	$.ajax({
		type: "post",
		url: url,
		dataType:"responseText",
		data: dataStr+"&"+isCheckBank+"&timeStamp="+(new Date().getTime()),
		success: function(result){
		var data = eval("("+result+")");
			if(data.result == "0") {
				var turnForm = document.createElement("form");   
			    document.body.appendChild(turnForm);
			    turnForm.method = 'post';
			    turnForm.action = 'check_succeed.jsp';
				 //创建隐藏表单
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","orderId");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.orderId);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","txMoney");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.txMoney);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","txDate");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.txDate);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","gateWayID");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.gateWayID);
			    turnForm.appendChild(newElement);
			    turnForm.submit();
			} else {
				$(".bg").css("display","none");
				document.getElementById('proess').style.display = "none";				
				$("#fade").show();
                $("#light_fail").show();
			}
		}
	});
}

function closeDialog1(){
        $("#fade").hide();
        $("#light_fail").hide();
}

function checkSuccess(url){
	var dataStr="orderKey="+$("#orderKey").val();
	var isCheckBank = "isCheckBank=1";
	$.ajax({
		type: "post",
		url: url,
		dataType:"responseText",
		data: dataStr+"&"+isCheckBank+"&timeStamp="+(new Date().getTime()),
		success: function(result){
		var data = eval("("+result+")");
			if(data.result == "0") {
				var turnForm = document.createElement("form");   
			    document.body.appendChild(turnForm);
			    turnForm.method = 'post';
			    turnForm.action = 'check_succeed.jsp';
				 //创建隐藏表单
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","orderId");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.orderId);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","txMoney");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.txMoney);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","txDate");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.txDate);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","gateWayID");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.gateWayID);
			    turnForm.appendChild(newElement);
			    
			    turnForm.submit();
			} if (data.result == "2") {
				var turnForm = document.createElement("form");   
			    document.body.appendChild(turnForm);
			    turnForm.method = 'post';
			    turnForm.action = 'cekWrong.jsp';
				 //创建隐藏表单
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","errorCode");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.errorCode);
			    turnForm.appendChild(newElement);
			    turnForm.submit();
			}
		}
	});
}

function checkPay(url){
	var wid = $(window).width();
	var hei = $(window).height();
	var lef = (wid - 600)/2+"px";
	var heii = (hei - 400)/2+"px";
	$(".terms").css({
		"left":lef,
		"top":heii
	})
	$(".bg").css({
	"opacity":0.7,
	"width":wid,
	"height":hei+200
	});
	$(".bg").css("display","block");
	document.getElementById('proess').style.display = "";
	$("#fade").hide();
    $("#light").hide();
	var dataStr="orderKey="+$("#orderKey").val();
	var isCheckBank = "isCheckBank=0";
	$.ajax({
		type: "post",
		url: url,
		dataType:"responseText",
		data: dataStr+"&"+isCheckBank+"&timeStamp="+(new Date().getTime()),
		success: function(result){
		var data = eval("("+result+")");
			if(data.result == "0") {
				var turnForm = document.createElement("form");   
			    document.body.appendChild(turnForm);
			    turnForm.method = 'post';
			    turnForm.action = 'check_succeed.jsp';
				 //创建隐藏表单
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","orderId");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.orderId);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","txMoney");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.txMoney);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","txDate");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.txDate);
			    turnForm.appendChild(newElement);
			    var newElement = document.createElement("input");
			    newElement.setAttribute("name","gateWayID");
			    newElement.setAttribute("type","hidden");
			    newElement.setAttribute("value",data.gateWayID);
			    turnForm.appendChild(newElement);
			    turnForm.submit();
			} else {
				$(".bg").css("display","none");
				document.getElementById('proess').style.display = "none";				
			}
		}
	});
}

function submitToErrorPage(errorCode){
	var form = $("<form></form>");
	form.attr("action","checkWrong.jsp");
	form.action = "checkWrong.jsp";
	var input = $("<input />");
	input.attr("name","errorCode");
	input.attr("value",errorCode);
	input.attr("type","hidden");
	form.append(input);
	$("body").append(form);
//	alert(form);
	form.submit();
}

if(!util){
	var util = {};
}
if(!util.page){
	util.page = function(){
	};
	util.page.prototype.disableContextMenu = function(){
		$(document).bind('contextmenu', function() {
      		return false;
    	});
	};
	util.page.prototype.init = function(){
		this.disableContextMenu();
	};
}
$(document).ready(function(){
   	var page = new util.page();
	page.init();
});
